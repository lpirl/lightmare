.. image:: https://gitlab.com/lpirl/lightmare/badges/master/pipeline.svg
  :target: https://gitlab.com/lpirl/lightmare/pipelines
  :align: right

This is an Openbox theme just like "Nightmare-01" but with minor
modifications (mainly higher visibility of window boarders and slightly
different icons).

`Install via .obt file <https://gitlab.com/lpirl/lightmare/-/jobs/artifacts/master/raw/lightmare.obt?job=pack>`__

or manually, e.g.,::

  $ mkdir -p ~/.local/share/themes
  $ git -C ~/.local/share/themes clone https://gitlab.com/lpirl/lightmare.git

And then, in your Openbox rc file,
set ``openbox_config -> theme -> name`` to `lightmare`.
